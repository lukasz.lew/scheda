// Currently the only way to run this code is to copy-paste it to script.google.com app and run main() function.

// You need access to this sheet.
testUrl = "https://docs.google.com/spreadsheets/d/16cZwWUxRNi5Ymfn9cXg9qOeuclpKCmRFmeVw2InJ0VQ/edit"


function recreateSheet(app, name) {
  var sheet = app.getSheetByName(name);
  if (sheet != null) app.deleteSheet(sheet);
  sheet = app.insertSheet();
  sheet.setName(name);
  return sheet;
}


function logMap(map) {
  Logger.log("logMap");
  map.forEach((value, key) => Logger.log(`${key}: ${value}\n`));
}


function error(msg) {
  Logger.log("Error: " + msg);
  SpreadsheetApp.getUi().alert(msg);
}


function getSheet(app, sheetName) {
  const sheet = app.getSheetByName(sheetName);
  if (sheet == null) error("Could not find sheet: " + sheetName);
  return sheet
}


function getColumnIndex(values, columnName) {
  const i = values[0].indexOf(columnName);
  if (i == -1) error("Can't find column '" + columnName +"' in sheet '" + range.getSheet().getName());
  return i;
}


// Assummes first row in the sheet are column names.
// getColumns(app, "My sheet", ["Date", "Name"]) would return array of rows, each row being 2 element array.
function getColumns(app, sheetName, columnNames) {
  var values = getSheet(app, sheetName).getDataRange().getValues();
  columnIndices = columnNames.map(columnName => getColumnIndex(values, columnName));
  values.shift();
  var ret = []
  for (value of values) {
    const row = columnIndices.map(i => value[i]);
    ret.push(row)
  }
  return ret;
}


// returns a map from group name to list of nicks, skips entries with empty group.
function computeGroups(app) {
  var ret = new Map();
  for ([nick, group] of getColumns(app, "Players", ["Nick", "Group"])) {
    Logger.log("Player:", nick, group);
    if (group == "") continue; // don't schedule players without a group
    if (ret.has(group)) {
      ret.get(group).push(nick);
    } else {
      ret.set(group, [nick]);
    }
  }
  return ret;
}


function createGameList(app, groups) {
  Logger.log("populateGameList");
  var gameListSheet = recreateSheet(app, "Game list")
  gameListSheet.getRange(1, 1).setValue("Group");
  gameListSheet.getRange(1, 2).setValue("Player 1");
  gameListSheet.getRange(1, 3).setValue("Player 2");
  gameListSheet.getRange(1, 4).setValue("Date and time");
  gameListSheet.getRange(1, 5).setValue("This sheet is automaticly generated, running scheduleGames() will recreate it.");
  // gameListSheet.getRange(1, 5).setValue(new Date('February 17, 2018 13:00:00 -0500'));


  var row = 2;
  logMap(groups);
  for ([group, nicks] of groups) {
    Logger.log("g", group, nicks);
    for (nick1 of nicks) {
      for (nick2 of nicks) {
        if (nick1 >= nick2) continue;
        Logger.log(nick1, nick2);
        gameListSheet.getRange(row, 1).setValue(group);
        gameListSheet.getRange(row, 2).setValue(nick1);
        gameListSheet.getRange(row, 3).setValue(nick2);
        // TODO: This algorithm needs to be replaced with code in roundRobin(n) function below.
        row += 1;
      }
    }
  }
}


function main() {
  Logger.log("scheduleGames");
  var app = SpreadsheetApp.openByUrl(testUrl);
  var groups = computeGroups(app);
  createGameList(app, groups);
}

// Python code with a correct Round Robin scheduler.

// def addMod(a, b, n):
//   return (a + b + n) % n

// def roundRobin(playerCount):
//   assert playerCount % 2 == 0

//   allGames = set() # Used only for testing
//   gamesByDay = []
//   def addGame(p1, p2):
//     assert p1 != p2
//     assert 0 <= p1 < playerCount
//     assert 0 <= p2 < playerCount
//     # RR algorithm makes sure that the players with largest numbers play last.
//     # This transformation makes sure that the players (0, 1, ...) are
//     p1 = playerCount - 1 - p1
//     p2 = playerCount - 1 - p2

//     game = (min(p1, p2), max(p1, p2)) # Sort 2 elements
//     gamesByDay[-1].append(game)
//     assert game not in allGames # no duplicate games
//     allGames.add(game)

//   for odd in range(playerCount - 1):
//     gamesByDay.append([])
//     addGame(odd, playerCount - 1)
//     for i in range(1, playerCount//2):
//       p1 = addMod(odd, i, playerCount - 1)
//       p2 = addMod(odd, -i, playerCount - 1)
//       addGame(p1, p2)

//   assert len(allGames) == playerCount*(playerCount - 1) // 2 # Tests that we have allGames the games
//   return gamesByDay

// print(roundRobin(6))
